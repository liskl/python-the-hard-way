#/usr/bin/env python3 python3
# -*- coding: utf-8 -*-

# this will print "Mary had a little lamb."
print("Mary had a little lamb.")

# this will print "It's Fleece was white as snow"
print("It's Fleece was white as %s.".format('snow'))

# this will print "and Everywhere that mery went."
print("and Everywhere that mery went.")

# this prints 10 periods on 1 line.
print("." * 10 ) # what did that do.

# sets Variables for concatenation later
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# this creates 1 line "Cheese Burger" by concatenation of all the end# variables.
# whatch that comma at the end remove it and see what happens.
print( end1 + end2 + end3 + end4 + end5 + end6 ),
print( end7 + end8 + end9 + end10 + end11 + end12 )
