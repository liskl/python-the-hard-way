#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date

# calculate a persons age, given born = "date(1984, 3, 3)"
def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

# calculate a persons height from inches to feet
def Inches2Feet(inches):
    feet = ( inches / 12.0 )
    return round(feet, 2);

def Pounds2Stone(pounds):
    # 1lbs = 0.0714286 stone.
    return (pounds * 0.0714286)

def Pounds2Kilogram(pounds):
    # 1lbs = 0.45359237 stone.
    return (pounds * 0.45359237)

name = "Loren R, Lisk"
age = calculate_age(date(1984, 3, 3))
height = Inches2Feet(72.5) #Feet
weight = 235 # lbs
eyes = 'Green'
teeth = 'White-ish'
hair = 'Dirty Blonde'


print("My Age is {}".format(age) ) # Not a Lie that shiz is Calulated
print("My Height is {} feet tall.".format(height))
print("My Weight is {:,d} lbs heavy.".format(weight))
print(" Which can also be said as \"My Weight is {:,.2f} stone heavy.\"".format(Pounds2Stone(weight)))
print(" Which can also be said as \"My Weight is {:,.2f} Kilograms heavy.\"".format(Pounds2Kilogram(weight)))

print("")
print("He's got {} eyes and {} hair.".format(eyes, hair))
print("His Teeth are generally pretty {}, depending on the effort expended.".format(teeth))
print("If I add my {}, {} and {}; I get {}".format(age, height, weight, ( age + height + weight )))
