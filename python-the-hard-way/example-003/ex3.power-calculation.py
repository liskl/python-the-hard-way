#!/usr/bin/python
# -*- coding: utf-8 -*-

import random
import csv

DEBUG = True

datafile = open('example-003/EnergyBillData.csv')
csvfile = csv.reader(datafile)
BillData = list(csvfile)


# "Date", "days in Cycle", "Used KiloWatt Hours", "Dollars Spent",
# "customer_charge", "nonfuel", "fuel", "storm", "gross_recipient",
# "franchise", "utility"

def percentage(percent, whole, name):
    percent = float(round(percent, 2))
    whole = float(round(whole, 2))
    name = str(name)
    calculated_value = round(percent * (100.0 / whole), 2)

    if DEBUG:
        calculation = str('').join([
            'Calc: ( ',
            str(percent),
            ' * ',
            '( 100 / ',
            str(whole),
            ' ) )',
            ])
        print 'DEBUG: {:}\tPercentage: {:,.2f}%,\t{:}'.format(name,
            calculated_value, calculation)
        return float(calculated_value)


def displayIfDebugEnabled(text, DEBUG=DEBUG):
    if DEBUG:
        return str(text)
    else:
        return str('')


for Bill in BillData:
    status = 'OK'

    # Base Factors for Future Calulations.

    date = str(Bill[0])
    days_in_cycle = int(Bill[1])  # Days this cycle
    kwh_in_cycle = int(Bill[2])  # Used_KWh this cycle
    cost_in_cycle = float(Bill[3])  # Bill $ this cycle
    customer_charge = float(Bill[4])
    nonfuel_charge = float(Bill[5])
    fuel_charge = float(Bill[6])
    storm_charge = float(Bill[7])
    gross_recipient_tax = float(Bill[8])
    franchise_charge = float(Bill[9])
    utility_tax = float(Bill[10])

    # Combine Charges and Taxes.

    taxes_and_fees = float(storm_charge + franchise_charge
                           + gross_recipient_tax + utility_tax)

    # Calculate Computed Cost of KWh Used.

    electricity_cost = float(customer_charge + nonfuel_charge
                             + fuel_charge)

    # Calculate the Total cost of Bill.

    calc_cost_in_cycle = float(electricity_cost + taxes_and_fees)

    # there is a computed difference between my Calulated total and FPL's

    if float(cost_in_cycle) != float(round(calc_cost_in_cycle, 2)):
        status = 'ERROR'
        print 'cost_in_cycle: '.format(str(cost_in_cycle))
        print 'calc_cost_in_cycle: '.format(str(calc_cost_in_cycle))

    calc_cost_per_kwh_str = str('').join([
        '((',
        str(cost_in_cycle),
        ' - ',
        str(taxes_and_fees),
        ' ) / ',
        str(kwh_in_cycle),
        ' )',
        ])
    calc_cost_per_hour_str = str('').join(['( ', str(cost_in_cycle),
            ' / ( ', str(days_in_cycle), ' * 24 ) )'])
    calc_cost_per_day_str = str('').join([str(calc_cost_per_hour_str),
            ' * 24', ')'])
    calc_cost_per_week_str = str('').join([str(calc_cost_per_day_str),
            ' * 7', ' )'])

    calc_kwh_per_hour_str = str('').join(['( ', str(days_in_cycle),
            ' * 24 ) / ', str(kwh_in_cycle), ' )'])
    calc_kwh_per_day_str = str('').join(['( ( ',
            str(calc_kwh_per_hour_str), ' * 24 )'])

    calc_kwh_per_week_str = str('').join(['( ',
            str(calc_kwh_per_day_str), ' * 7 )'])

    calc_cost_per_kwh = float(round(kwh_in_cycle / (days_in_cycle
                              * 24), 2))
    calc_cost_per_hour = float(round(cost_in_cycle / (days_in_cycle
                               * 24), 2))
    calc_cost_per_day = float(round(calc_cost_per_hour * 24, 2))
    calc_cost_per_week = float(round(calc_cost_per_day * 7, 2))

    calc_kwh_per_hour = float(round(days_in_cycle * 24 / kwh_in_cycle,
                              2))
    calc_kwh_per_day = float(round(calc_kwh_per_hour * 24, 2))
    calc_kwh_per_week = float(round(calc_kwh_per_day * 7, 2))

    print 'DATE: {:}'.format(str(date))
    print "status: {:}, total_bill: '${:,.2f}',".format(status,
            cost_in_cycle)
    print '#############################################################'

    if DEBUG:
        fee_percentage_in_cycle = percentage(taxes_and_fees,
                cost_in_cycle, 'taxes_and_fees')
        fuel_percentage_in_cycle = percentage(fuel_charge,
                cost_in_cycle, 'fuel_charge')
        nonfuel_percentage_in_cycle = percentage(nonfuel_charge,
                cost_in_cycle, 'nonfuel_charge')
        total_calculation_in_cycle = percentage(calc_cost_in_cycle,
                cost_in_cycle, 'cost_in_cycle')

    print 'Calulated Costs:'
    print '\tAvg Cost per KWH:\t${:} USD\t{:}'.format(str(calc_cost_per_kwh).zfill(5),
            displayIfDebugEnabled('Calc: ' + calc_cost_per_kwh_str))
    print '\tAvg Cost per Hour:\t${:} USD\t{:}'.format(str(calc_cost_per_hour).zfill(5),
            displayIfDebugEnabled('Calc: ' + calc_cost_per_hour_str))
    print '\tAvg Cost per Day:\t${:} USD\t{:}'.format(str(calc_cost_per_day).zfill(5),
            displayIfDebugEnabled('Calc: ' + calc_cost_per_day_str))
    print '\tAvg Cost per Week:\t${:} USD\t{:}'.format(str(calc_cost_per_week).zfill(5),
            displayIfDebugEnabled('Calc: ' + calc_cost_per_week_str))
    print ''
    print '\tAvg KWh per Hour:\t{:} KWh\t{:}'.format(str(calc_kwh_per_hour).zfill(5),
            displayIfDebugEnabled('Calc: ' + calc_kwh_per_hour_str))
    print '\tAvg KWh per Day:\t{:} KWh\t{:}'.format(str(calc_kwh_per_day).zfill(5),
            displayIfDebugEnabled('Calc: ' + calc_kwh_per_day_str))
    print '\tAvg KWh per Week:\t{:} KWh\t{:}'.format(str(calc_kwh_per_week).zfill(5),
            displayIfDebugEnabled('Calc: ' + calc_kwh_per_week_str))
    print '#############################################################'

    print ''
