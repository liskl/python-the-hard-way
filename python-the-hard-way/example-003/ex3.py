#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Math examples with print.

# prints "I will now count my chickens:"
print( "I will now count my chickens:" )

# prints "Hens:  30.0"
print( "Hens: ", 25 + 30 / 6 )

# prints "Roosters:  97"
print( "Roosters: ", 100 - 25 * 3 % 4 )

# prints "Now I Will count the eggs:"
print( "Now I Will count the eggs:" )

# prints "6.75"
print( 3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6 )

# prints "False"
print( 3 + 2 < 5 - 7 )

# prints "What is 3 + 2?  5"
print( "What is 3 + 2? ", 3 + 2 )

# prints "What is 5 - 7?  -2"
print( "What is 5 - 7? ", 5 - 7 )

# prints "Oh, that's why it's False."
print( "Oh, that's why it's False." )

# prints "How about some more."
print( "How about some more." )

# prints "Is it greater?  True"
print( "Is it greater? ", 5 > -2 )

# prints "Is it greater or equal?  True"
print( "Is it greater or equal? ", 5 >= -2 )

# prints "Is it less or equal?  False"
print( "Is it less or equal? ", 5 <= -2 )
