#/usr/bin/env python3 python3
# -*- coding: utf-8 -*-

# TITLE: Python: The hard way, python3 Edition.
# DISCRIPTION: Python the hard way mod for python3: Example 8

formatter = "%r %r %r %r"

print formatter % (1, 2, 3, 4)
print formatter % ("one","two","three","four")
print formatter % (True, False, False, True)
print formatter % (formatter, formatter, formatter, formatter)
print formatter % (
    "I had this thing.",
    "That you could type up right.",
    "But it didn't sing.",
    "So I said goodnight."
)
