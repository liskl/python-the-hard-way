#!/usr/bin/env python3
# -*- coding: utf-8 -*-

x = "There are %d types of people." % 10
binary = "Binary"
do_not = "don't"
y = "Those who know %s and those who %s." % (binary, do_not)

print(x)
print(y)

print("I said: {}.".format( x ))
print("I also said: '{}'.".format(y))

hilarious = False
joke_evaluation = "Isn't that a joke so funny?! %r"

print(joke_evaluation.format(hilarious))

w = "This is the left side of..."
e = "a string with a right side."

print(w + e)
